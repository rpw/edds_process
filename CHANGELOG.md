CHANGELOG
====================

0.10.1
------
- Minor updates in python deps

0.10.0
-------
- Update license (CeCILL 2.1)
- Update Python dev dependencies in pyproject.toml

0.9.1
-----
- Installation update

0.9.0
------
- Update unit tests
- Minor hotfixes

0.8.4
------
- Update pyproject.toml and poetry.lock

0.8.3
-----
- Add the capability to deploy the package in gitlab via .gitlab-ci.yml
- Change license to CeCILL-C

0.8.2
------
- Make sure packets input is a list in make_tmraw_xml() method

0.8.1
-----
- Update setup env

0.8.0
-----
* Add to_list optional keyword in xml_to_dict() method in response.parse
* Use MIT LICENSE

0.7.0
-----
* Introduce PEP518 installation mechanism

0.6.2
-----
* Add make_param_xml() method

0.6.1
-----
* Rename etree_to_dict() to xml_to_dict() and use dicttoxml module instead
* Use xmltodict.unparse() method instead of jinja2.render() to make TcReport XML

0.6.O
-----
* Add etree_to_dict() method and related xml_to_dict classes.

0.5.2
-----
* Make file type fetching not case sensitive in count_packets()

0.5.1
-----
* Replace is_tcreport by file_type keyword in count_packets() method

0.5.0
-----
* Add the count_packets() method

0.4.0
-----
* Add make_tcreport_xml method to generate 'tcreport-like' file (all expected fields are not provided)

0.3.3
-----
* Add description in tc_report tag

0.3.2
-----
* Use CommandName and ExecutionTime as keywords for tc_report dict()

0.3.1
-----
* Minor changes

0.3.0
-----
* Add unit tests in test_response.py
* Return tc_report as dictionary in parse.parse_tcreport_xml() method
* Remove utils.py and Element class

0.2.1
-----
* Minor fixes in utils.py

0.2.0
-----
* Add yield_tcreport_xml and yield_raw_xml methods

0.1.0
-----
* First release
