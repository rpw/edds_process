edds_process
============

Python Package to handle EGOS Data Dissemination System (EDDS) Data.

## Quickstart

To install package using [pip](https://pypi.org/project/pip-tools/):

```
pip install edds_process
```

## User guide

Package modules can be then imported into codes with line `import edds_process`.

Especially, `edds_process.response` module contains methods to parse/create TmRaw/TcReport EDDS XML files.

## Authors

- xavier dot bonnin at obspm dot fr
